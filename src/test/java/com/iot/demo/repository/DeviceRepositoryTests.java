package com.iot.demo.repository;

import com.iot.demo.entity.Device;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql({"/test-data.sql"})
public class DeviceRepositoryTests {

  @Autowired private DeviceRepository deviceRepository;

  @Test
  void findDeviceById_shouldReturnOptionalDevice() {
    Optional<Device> optionalDevice = deviceRepository.findById(1);
    Assertions.assertNotNull(optionalDevice);
    Assertions.assertTrue(optionalDevice.isPresent());
  }
}
