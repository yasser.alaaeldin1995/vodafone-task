package com.iot.demo.service;

import com.iot.demo.entity.Device;
import com.iot.demo.exception.DeviceNotFoundException;
import com.iot.demo.repository.DeviceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql({"/test-data.sql"})
class DeviceServiceTests {

  @Autowired private DeviceService deviceService;

  @Autowired private DeviceRepository deviceRepository;

  @Test
  void gettingValidDeviceByID_returnsDeviceObject() {
    Device device = deviceService.getDevice(1);
    Assertions.assertNotNull(device);
  }

  @Test
  void gettingNonExistentDevice_deviceNotFoundExceptionThrown() {
    Assertions.assertThrows(
        DeviceNotFoundException.class,
        () -> {
          deviceService.getDevice(20);
        });
  }

  @Test
  void whenGettingValidDevice_thenValidValuesShouldReturn() {
    Device device = deviceService.getDevice(1);

    Assertions.assertEquals(1, device.getId());
    Assertions.assertEquals("NOT READY", device.getStatus());
    Assertions.assertEquals(20, device.getTemperature());
    Assertions.assertEquals(1, device.getSim().getId());
  }

  @Test
  void gettingAllWaitingForActivationDevices_shouldHaveDevices() {
    List<Device> deviceList = deviceService.getWaitingForActivationDevices(0, 15);
    Assertions.assertTrue(deviceList.size() > 0);
  }

  @Test
  void gettingAllWaitingForActivationDevices_shouldHaveValidStatus() {
    List<Device> deviceList = deviceService.getWaitingForActivationDevices(0, 15);
    for (Device device : deviceList) {
      Assertions.assertEquals("Waiting for activation", device.getSim().getStatus());
    }
  }

  @Test
  void gettingAllWaitingForActivationDevices_shouldNotReturnNull() {
    List<Device> deviceList = deviceService.getWaitingForActivationDevices(5, 15);
    Assertions.assertNotNull(deviceList);
  }

  @Test
  void testUpdateDeviceConfiguration() {
    Optional<Device> optionalDevice = deviceRepository.findById(1);

    Assertions.assertTrue(optionalDevice.isPresent());

    Device device = optionalDevice.get();

    String status1 = device.getStatus();
    deviceService.updateDeviceStatus(1, "READY");

    optionalDevice = deviceRepository.findById(1);
    device = optionalDevice.get();
    String status2 = device.getStatus();
    Assertions.assertNotEquals(status1, status2);
  }

  @Test
  void onDeletingDevice_shouldBeDeletedFromDB() {

    Device device = deviceService.getDevice(1);
    Assertions.assertNotNull(device);

    deviceService.deleteDevice(1);
    Assertions.assertThrows(
        DeviceNotFoundException.class,
        () -> {
          deviceService.getDevice(1);
        });
  }

  @Test
  void getAllDevicesAvailableForSale_shouldReturnValidDevices() {
    List<Device> deviceList = deviceService.getAvailableDevicesForSale(0, 15);
    Assertions.assertNotNull(deviceList);
    Assertions.assertTrue(deviceList.size() > 0);
    for (Device device : deviceList) {
      Assertions.assertEquals("England", device.getSim().getCountry());
      Assertions.assertEquals("READY", device.getStatus());
      Assertions.assertTrue(device.getTemperature() >= -25 && device.getTemperature() <= 85);
    }
  }
}
