package com.iot.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iot.demo.VIotApplication;
import com.iot.demo.dto.DeviceStatusDto;
import com.iot.demo.entity.Device;
import com.iot.demo.repository.DeviceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.Optional;

@SpringBootTest(classes = VIotApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@Sql({"/test-data.sql"})
public class DeviceControllerTests {

  @Autowired private MockMvc mvc;

  @Autowired private DeviceRepository deviceRepository;

  protected String mapToJson(Object obj) throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(obj);
  }

  protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {

    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.readValue(json, clazz);
  }

  @Test
  public void callingGetDevice_shouldProvideFullDeviceData_thenStatus200() throws Exception {
    String uri = "/device/1";
    MvcResult mvcResult =
        mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
            .andReturn();
    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(200, status);

    String content = mvcResult.getResponse().getContentAsString();
    Device device = mapFromJson(content, Device.class);
    Assertions.assertNotNull(device);
    Assertions.assertNotNull(device.getId());
    Assertions.assertNotNull(device.getSim());
    Assertions.assertNotNull(device.getStatus());
    Assertions.assertNotNull(device.getTemperature());
  }

  @Test
  public void callingGetDeviceWithInvalidID_shouldReturnStatus404() throws Exception {
    String uri = "/device/20";
    MvcResult mvcResult =
        mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
            .andReturn();
    int status = mvcResult.getResponse().getStatus();
    String message = mvcResult.getResponse().getContentAsString();
    Assertions.assertEquals(404, status);
    Assertions.assertEquals("Device not found", message);
  }

  @Test
  public void callingDeleteDevice_shouldRemoveDeviceFromDBAndReturnStatus200() throws Exception {
    String uri = "/device/10";
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(200, status);

    Assertions.assertEquals(false, deviceRepository.existsById(10));
  }

  @Test
  public void callingDeleteDeviceWithInvalidID_shouldReturnStatus404() throws Exception {
    String uri = "/device/30";
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
    int status = mvcResult.getResponse().getStatus();
    String message = mvcResult.getResponse().getContentAsString();
    Assertions.assertEquals(404, status);
    Assertions.assertEquals("Device not found", message);
  }

  @Test
  public void callingDeviceWithValidIDAndPath_shouldUpdateStatusAndGetStatus200() throws Exception {
    Optional<Device> optionalDevice = deviceRepository.findById(1);

    Assertions.assertTrue(optionalDevice.isPresent());

    Device device = optionalDevice.get();

    Assertions.assertEquals("NOT READY", device.getStatus());

    DeviceStatusDto deviceStatusDto = new DeviceStatusDto();
    deviceStatusDto.setStatus("READY");

    String inputJson = mapToJson(deviceStatusDto);

    String uri = "/device/1";
    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.patch(uri)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(inputJson))
            .andReturn();

    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(200, status);

    optionalDevice = deviceRepository.findById(1);

    Assertions.assertTrue(optionalDevice.isPresent());

    device = optionalDevice.get();

    Assertions.assertEquals("READY", device.getStatus());
  }

  @Test
  public void
      callingWaitingForActivationDevices_shouldProvideDevicesWithValidSimStatus_thenStatus200()
          throws Exception {
    String uri = "/device/watingForActivation?pageNo=0&pageSize=5";
    MvcResult mvcResult =
        mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
            .andReturn();
    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(200, status);

    String content = mvcResult.getResponse().getContentAsString();
    Device[] deviceList = mapFromJson(content, Device[].class);
    Assertions.assertTrue(deviceList.length > 0);

    for (Device device : deviceList) {
      Assertions.assertEquals("Waiting for activation", device.getSim().getStatus());
    }
  }

  @Test
  public void callingAvailableForSale_shouldAllStandardDevices_thenStatus200() throws Exception {
    String uri = "/device/availableForSale?pageNo=0&pageSize=5";
    MvcResult mvcResult =
        mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
            .andReturn();
    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(200, status);
    String content = mvcResult.getResponse().getContentAsString();
    Device[] deviceList = mapFromJson(content, Device[].class);
    Assertions.assertTrue(deviceList.length > 0);

    for (Device device : deviceList) {
      Assertions.assertEquals("England", device.getSim().getCountry());
      Assertions.assertEquals("READY", device.getStatus());
      Assertions.assertTrue(device.getTemperature() >= -25 && device.getTemperature() <= 85);
    }
  }
}
