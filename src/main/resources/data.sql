DROP TABLE IF EXISTS devices;
DROP TABLE IF EXISTS sims;


CREATE TABLE sims (
    sim_id INT AUTO_INCREMENT  PRIMARY KEY,
    operator_id VARCHAR(250) NOT NULL,
    country VARCHAR(250) NOT NULL,
    status VARCHAR(250) NOT NULL
);

INSERT INTO sims (sim_id, operator_id, country, status) VALUES
(1, 'V123', 'Egypt', 'Active'),
(2, 'E123', 'England', 'Waiting for activation'),
(3, 'O123', 'Italy', 'Blocked'),
(4, 'V123', 'Spain', 'Deactivated'),
(5, 'E123', 'England', 'Waiting for activation'),
(6, 'V123', 'England', 'Waiting for activation'),
(7, 'O123', 'England', 'Deactivated'),
(8, 'E123', 'Finland', 'Waiting for activation'),
(9, 'V123', 'England', 'Waiting for activation'),
(10, 'O123', 'Germany', 'Waiting for activation'),
(11, 'E123', 'England', 'Blocked'),
(12, 'V123', 'England', 'Waiting for activation'),
(13, 'O123', 'England', 'Waiting for activation'),
(14, 'E123', 'England', 'Active'),
(15, 'V123', 'England', 'Waiting for activation');

CREATE TABLE devices (
    device_id INT AUTO_INCREMENT  PRIMARY KEY,
    status VARCHAR(250) NOT NULL,
    temperature INT NOT NULL,
    sim_id INT NOT NULL,
    FOREIGN KEY(sim_id) REFERENCES sims(sim_id)
);

INSERT INTO devices (device_id, status, temperature, sim_id) VALUES
(1, 'NOT READY', 20, 1),
(2, 'READY', -22, 2),
(3, 'READY', 85, 3),
(4, 'READY', 80, 4),
(5, 'NOT READY', 20, 5),
(6, 'READY', 1, 6),
(7, 'NOT READY', 20, 7),
(8, 'READY', 20, 8),
(9, 'NOT READY', 20, 9),
(10, 'NOT READY', 20, 10),
(11, 'NOT READY', 22, 11),
(12, 'READY', 20, 12),
(13, 'NOT READY', 15, 13),
(14, 'NOT READY', -12, 14),
(15, 'READY', -55, 15);