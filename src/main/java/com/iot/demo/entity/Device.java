package com.iot.demo.entity;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Table(name = "devices")
@Transactional
@Entity
public class Device {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "device_id")
  private Integer id;

  @Column private String status;

  @Column private Integer temperature;

  @OneToOne(cascade = CascadeType.DETACH)
  @JoinColumn(name = "sim_id", referencedColumnName = "sim_id")
  private Sim sim;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Integer getTemperature() {
    return temperature;
  }

  public void setTemperature(Integer temperature) {
    this.temperature = temperature;
  }

  public Sim getSim() {
    return sim;
  }

  public void setSim(Sim sim) {
    this.sim = sim;
  }

  @Override
  public String toString() {
    return "Device{"
        + "id='"
        + id
        + '\''
        + ", status='"
        + status
        + '\''
        + ", temperature="
        + temperature
        + ", sim="
        + sim
        + '}';
  }
}
