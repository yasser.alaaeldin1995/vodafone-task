package com.iot.demo.entity;

import javax.persistence.*;

@Table(name = "sims")
@Entity
public class Sim {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "sim_id")
  private Integer id;

  @Column(name = "operator_id")
  private String operatorCode;

  @Column private String country;

  @Column private String status;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getOperatorCode() {
    return operatorCode;
  }

  public void setOperatorCode(String operatorCode) {
    this.operatorCode = operatorCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Sim{"
        + "id='"
        + id
        + '\''
        + ", operatorCode='"
        + operatorCode
        + '\''
        + ", country='"
        + country
        + '\''
        + ", status='"
        + status
        + '\''
        + '}';
  }
}
