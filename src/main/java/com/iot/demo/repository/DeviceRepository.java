package com.iot.demo.repository;

import com.iot.demo.entity.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends PagingAndSortingRepository<Device, Integer> {
  Page<Device> findDevicesBySimStatus(String status, Pageable pageable);

  Page<Device> findDevicesByStatusAndTemperatureBetweenAndSimCountry(
      String status,
      Integer temperatureFrom,
      Integer temperatureTo,
      String country,
      Pageable pageable);
}
