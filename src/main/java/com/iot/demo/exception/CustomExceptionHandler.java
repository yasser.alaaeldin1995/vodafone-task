package com.iot.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(DeviceNotFoundException.class)
  public final ResponseEntity<String> handleDeviceNotFoundException(
      DeviceNotFoundException deviceNotFoundException, WebRequest request) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Device not found");
  }
}
