package com.iot.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VIotApplication {

  public static void main(String[] args) {
    SpringApplication.run(VIotApplication.class, args);
  }
}
