package com.iot.demo.service;

import com.iot.demo.entity.Device;
import com.iot.demo.exception.DeviceNotFoundException;
import com.iot.demo.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class DeviceService {

  @Autowired private DeviceRepository deviceRepository;

  @Transactional(readOnly = true)
  public Device getDevice(Integer deviceId) {

    // check if deviceId doesn't exist
    if (!deviceRepository.existsById(deviceId)) throw new DeviceNotFoundException();

    Optional<Device> optionalDevice = deviceRepository.findById(deviceId);

    return optionalDevice.get();
  }

  @Transactional(readOnly = true)
  public List<Device> getWaitingForActivationDevices(Integer pageNo, Integer pageSize) {

    Pageable paging = PageRequest.of(pageNo, pageSize);

    Page<Device> devicesPage =
        deviceRepository.findDevicesBySimStatus("Waiting for activation", paging);

    if (devicesPage.hasContent()) {
      return devicesPage.getContent();
    } else {
      return new ArrayList<>();
    }
  }

  @Transactional(readOnly = true)
  public List<Device> getAvailableDevicesForSale(Integer pageNo, Integer pageSize) {

    Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("id"));

    Page<Device> devicesPage =
        deviceRepository.findDevicesByStatusAndTemperatureBetweenAndSimCountry(
            "READY", -25, 85, "England", paging);

    if (devicesPage.hasContent()) {
      return devicesPage.getContent();
    } else {
      return new ArrayList<>();
    }
  }

  @Transactional
  public void deleteDevice(Integer deviceId) {

    // check if deviceId doesn't exist
    if (!deviceRepository.existsById(deviceId)) throw new DeviceNotFoundException();

    Optional<Device> optionalDevice = deviceRepository.findById(deviceId);

    deviceRepository.delete(optionalDevice.get());
  }

  @Transactional
  public synchronized Device updateDeviceStatus(Integer deviceId, String status) {

    // check if deviceId doesn't exist
    if (!deviceRepository.existsById(deviceId)) throw new DeviceNotFoundException();

    Optional<Device> optionalDevice = deviceRepository.findById(deviceId);

    optionalDevice.get().setStatus(status);

    deviceRepository.save(optionalDevice.get());

    return optionalDevice.get();
  }
}
