package com.iot.demo.dto;

public class DeviceStatusDto {
  private String status;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
