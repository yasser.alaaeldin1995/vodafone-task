package com.iot.demo.controller;

import com.iot.demo.dto.DeviceStatusDto;
import com.iot.demo.entity.Device;
import com.iot.demo.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DeviceController {

  @Autowired private DeviceService deviceService;

  @RequestMapping(
      value = "/device/{deviceId}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public Device getDevice(@PathVariable Integer deviceId) {
    return deviceService.getDevice(deviceId);
  }

  @RequestMapping(
      value = "/device/{deviceId}",
      method = RequestMethod.DELETE,
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public void deleteDevice(@PathVariable Integer deviceId) {
    deviceService.deleteDevice(deviceId);
  }

  @RequestMapping(
      value = "/device/{deviceId}",
      method = RequestMethod.PATCH,
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public Device updateDeviceStatus(
      @PathVariable Integer deviceId, @RequestBody DeviceStatusDto deviceStatusDto) {
    return deviceService.updateDeviceStatus(deviceId, deviceStatusDto.getStatus());
  }

  @RequestMapping(
      value = "/device/watingForActivation",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public List<Device> getWaitingForActivationDevices(
      @RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize) {
    return deviceService.getWaitingForActivationDevices(pageNo, pageSize);
  }

  @RequestMapping(
      value = "/device/availableForSale",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public List<Device> getAvailableDevicesForSale(
      @RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize) {
    return deviceService.getAvailableDevicesForSale(pageNo, pageSize);
  }
}
