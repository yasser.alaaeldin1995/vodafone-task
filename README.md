Devices Shop
=============

**Description**

    - Backend API for tracking IoT devices.
    - Restrictions for selling devices meets UK standards only.
    
**Features: 3 main endpoints**
  
    - Returns all devices in the warehouse that are waiting for activation.
    - Management endpoints that enable the shop manager to remove or update a device configuration status.
    - Returns an ordered result of devices available for sale.
    - Test coverage for endpoints and scenarios.
    - InMemory database for prod and for testing.
    - Start from: running test cases in test files


**Libraries and frameworks**

    - Java
    - SpringBoot
    - H2 Database
    
